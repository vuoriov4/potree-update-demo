import b0RemoteApi
import time
import struct
import requests
import math
import numpy as np

LIDAR_CHANNEL = "LidarChannel"
OBJECTMATRIX_CHANNEL = "ObjectMatrixChannel"
API_ENDPOINT = 'http://localhost:5000'

def sendData(lidarData, objectMatrix):
    nparray = np.reshape(lidarData, (-1, 3))
    absolute = relativeToAbsolute(nparray, objectMatrix)
    x = absolute[:,0].flatten().tolist()[0]
    y = absolute[:,1].flatten().tolist()[0]
    z = absolute[:,2].flatten().tolist()[0]
    data = { 'x': x, 'y': y, 'z': z }
    headers = {'content-type': 'application/json'}
    response = requests.post(API_ENDPOINT + "/data", json=data, headers=headers)

def getTarget():
    response = requests.get(API_ENDPOINT + "/target")
    data = response.json()
    return data

def relativeToAbsolute(array, objectMatrix):
    m = np.array([
        (objectMatrix[0], objectMatrix[1], objectMatrix[2], objectMatrix[3]),
        (objectMatrix[4], objectMatrix[5], objectMatrix[6], objectMatrix[7]),
        (objectMatrix[8], objectMatrix[9], objectMatrix[10], objectMatrix[11]),
        (0, 0, 0, 1)
    ])
    cartesian = np.hstack((array, np.ones((len(array), 1))))
    return (np.asmatrix(m) * np.asmatrix(cartesian).T).T

def simxUnpackFloats(floatsPackedInString):
    b=[]
    for i in range(int(len(floatsPackedInString)/4)):
        b.append(struct.unpack('<f',floatsPackedInString[4*i:4*(i+1)])[0])
    return b

def convertToAbsolute(x, y, z, objectMatrix):
    Xx = objectMatrix[0]
    Yx = objectMatrix[1]
    Zx = objectMatrix[2]
    Px = objectMatrix[3]
    Xy = objectMatrix[4]
    Yy = objectMatrix[5]
    Zy = objectMatrix[6]
    Py = objectMatrix[7]
    Xz = objectMatrix[8]
    Yz = objectMatrix[9]
    Zz = objectMatrix[10]
    Pz = objectMatrix[11]
    ax = Xx * x + Yx * y + Zx * z + Px
    ay = Yy * x + Yy * y + Zy * z + Py
    az = Xz * x + Yz * y + Zz * z + Pz
    return [ax, ay, az]


def move(v, w, leftMotorHandle, rightMotorHandle):
    L = 0.24 # distance between wheels
    R = 0.04 # radius of wheel
    vl = (v - w*L/2.0)
    vr = (v + w*L/2.0)
    wl = vl / R
    wr = vr / R
    errorCode = client.simxSetJointTargetVelocity(leftMotorHandle, wl, client.simxServiceCall())
    if (errorCode == False): raise ValueError("Simx call failed.")
    errorCode = client.simxSetJointTargetVelocity(rightMotorHandle, wr, client.simxServiceCall())
    if (errorCode == False): raise ValueError("Simx call failed.")

def moveToTarget(objectMatrix, target, leftMotorHandle, rightMotorHandle):
    objectMatrixX = [objectMatrix[0], objectMatrix[4], objectMatrix[8]]
    objectMatrixY = [objectMatrix[1], objectMatrix[5], objectMatrix[9]]
    objectMatrixZ = [objectMatrix[2], objectMatrix[6], objectMatrix[10]]
    objectMatrixP = [objectMatrix[3], objectMatrix[7], objectMatrix[11]]
    t = [ (target[0] - objectMatrixP[0]), (target[1] - objectMatrixP[1]) ]
    tlen = math.sqrt(t[0]*t[0] + t[1]*t[1])
    t[0] = t[0] / tlen
    t[1] = t[1] / tlen
    d = [ objectMatrixX[0], objectMatrixX[1] ]
    dot = t[0] * d[0] + t[1] * d[1]
    cross = t[0] * d[1] - t[1] * d[0]
    if (tlen < 0.1):
        # stop
        move(0, 0, leftMotorHandle, rightMotorHandle)
        return False
    elif (abs(cross) < 0.1):
        # move forward
        move(0.2, 0.0, leftMotorHandle, rightMotorHandle)
        return True
    elif (cross < 0):
        # turn
        move(0.0, math.pi/8, leftMotorHandle, rightMotorHandle)
        return True
    else:
        # turn
        move(0.0, -math.pi/8, leftMotorHandle, rightMotorHandle)
        return True

with b0RemoteApi.RemoteApiClient('b0RemoteApi_pythonClient','b0RemoteApi',60) as client:
    print("Connected.")
    clientId = client.simxStartSimulation(client.simxServiceCall())
    leftMotorHandle = client.simxGetObjectHandle("LeftMotor", client.simxServiceCall())
    if (leftMotorHandle[0] == False): raise ValueError("Failed to get left motor handle.")
    rightMotorHandle = client.simxGetObjectHandle("RightMotor", client.simxServiceCall())
    if (rightMotorHandle[0] == False): raise ValueError("Failed to get right motor handle.")

    while (True):
        # Target movement
        robotHandle = client.simxGetObjectHandle("Robot", client.simxServiceCall())
        if (robotHandle[0] == False): raise ValueError("Failed to get robot handle.")
        objectMatrix = client.simxGetObjectMatrix(robotHandle[1], -1, client.simxServiceCall())
        if (objectMatrix[0] == False): raise ValueError("Failed to get object matrix.")
        target = getTarget()
        if (not (target == None)):
            moving = moveToTarget(objectMatrix[1], target, leftMotorHandle[1], rightMotorHandle[1])
        else:
            moving = False
        # Lidar data
        lidarSignal = client.simxGetStringSignal(LIDAR_CHANNEL, client.simxServiceCall())
        if (lidarSignal[0] == False or lidarSignal[1] == None): raise ValueError('Failed to get lidar signal.')
        lidarData = simxUnpackFloats(lidarSignal[1])
        objectMatrixSignal = client.simxGetStringSignal(OBJECTMATRIX_CHANNEL, client.simxServiceCall())
        if (objectMatrixSignal[0] == False or objectMatrixSignal[1] == None): raise ValueError('Failed to get object matrix signal.')
        objectMatrix = simxUnpackFloats(objectMatrixSignal[1])
        sendData(lidarData, objectMatrix)
        time.sleep(1.0)
    client.simxStopSimulation(client.simxServiceCall())
