{
    "version": "1.8",
    "octreeDir": "data",
    "projection": "",
    "points": 494,
    "boundingBox": {
        "lx": 1.21,
        "ly": -0.73,
        "lz": -0.16,
        "ux": 3.0700000000000005,
        "uy": 1.1300000000000002,
        "uz": 1.7000000000000002
    },
    "tightBoundingBox": {
        "lx": 1.21,
        "ly": -0.73,
        "lz": -0.16,
        "ux": 2.56,
        "uy": 1.1300000000000002,
        "uz": 0.68
    },
    "pointAttributes": "LAS",
    "spacing": 0.0,
    "scale": 0.001,
    "hierarchyStepSize": 5
}