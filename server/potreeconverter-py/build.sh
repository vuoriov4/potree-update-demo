cd PotreeConverter/PotreeConverter
g++ ./src/*.cpp -shared -o ../../libpotreeconverter.so -fPIC -std=c++17 -Iinclude -Ilib/rapidjson/include -Ilib/arguments -I../../LAStools/LASzip/dll -L../../LAStools/LASzip/build/src -llaszip -lstdc++fs -pthread
