from ctypes import *
import sys
import os

dirpath = os.path.dirname(os.path.abspath(__file__))
laszip = CDLL(dirpath + "/LAStools/LASzip/build/src/liblaszip.so", mode = RTLD_GLOBAL)
cmain = CDLL(dirpath + "/libpotreeconverter.so").main
cmain.argtypes = [c_char_p, c_char_p, c_char_p] # arguments types
cmain.restype = c_int # return type, or None if void

def convert(outputDirectory, sourcePath, incremental=False):
    #if (not os.path.isdir(outputDirectory)):
    #    raise Exception("Output directory " + outputDirectory + " does not exist.")
    if (not os.path.isfile(sourcePath)):
        raise Exception("Source path " + sourcePath + " does not exist.")
    executablePath = dirpath + "/PotreeConverter/build/PotreeConverter"
    cmain(
        create_string_buffer(executablePath.encode('utf-8')),
        create_string_buffer(outputDirectory.encode('utf-8')),
        create_string_buffer(sourcePath.encode('utf-8')),
        c_bool(incremental)
    )
