from flask import Flask, jsonify, request, Response
from flask_cors import cross_origin, CORS
from math import *
import json
import time
import laspy
import numpy as np
import os
import sys
sys.path.append("./potreeconverter-py")
from PotreeConverter import convert

app = Flask(__name__, static_url_path='', static_folder='')
CORS(app)
dirpath = os.path.dirname(os.path.abspath(__file__))
cloudId = None
target = None
PORT = 5000
API_ENDPOINT = "http://localhost:" + str(PORT)
totalPoints = {'x': [], 'y': [], 'z': []}

@app.after_request
def afterRequest(response):
    response.headers.add('Access-Control-Allow-Headers', 'Content-Type, Authorization')
    response.headers.add('Access-Control-Allow-Methods', 'GET, POST, PATCH, DELETE, OPTIONS')
    return response

@app.route('/data', methods = ['POST'])
def receiveData():
    data = request.get_json()
    print("Received data (" + str(len(data['x'])) + ")")
    totalPoints['x'] = totalPoints['x'] + data['x']
    totalPoints['y'] = totalPoints['y'] + data['y']
    totalPoints['z'] = totalPoints['z'] + data['z']
    #writeLas(data)
    writeLas(totalPoints)
    convertPotree()
    return jsonify(success = True)

@app.route('/target', methods = ['POST'])
def receiveTarget():
    global target
    data = request.get_json()
    target = [data['x'], data['y']]
    return jsonify(success = True)

@app.route('/target', methods = ['GET'])
def sendTarget():
    return jsonify(target)

@app.route("/stream")
@cross_origin()
def sendUpdateData():
    def eventStream():
        while True:
            if (cloudId == None):
                data = '{"success": false}'
            else:
                with open(dirpath + "/output_updates/" + cloudId + "/cloud.js") as f:
                    data = json.load(f)
                    data["success"] = True
                    data["cloudId"] = cloudId
                    data["url"] = API_ENDPOINT + "/output_updates/" + cloudId
                    data["octreeDir"] = data["url"] + "/data"
                    data = json.dumps(data)
            yield "data: {}\n\n".format(data)
            time.sleep(1.0)
    return Response(eventStream(), mimetype="text/event-stream")

def writeLas(data):
    header = laspy.header.Header(point_format=2)
    outfile = laspy.file.File(dirpath + "/las/points.las", mode="w", header=header)
    xs = np.array(data['x'])
    ys = np.array(data['y'])
    zs = np.array(data['z'])
    xmin = np.min(xs)
    ymin = np.min(ys)
    zmin = np.min(zs)
    xmax = np.max(xs)
    ymax = np.max(ys)
    zmax = np.max(zs)
    size = len(data['x'])
    rs = np.empty(size)
    rs.fill(65535)
    gs = np.empty(size)
    gs.fill(65535)
    bs = np.empty(size)
    bs.fill(65535)
    outfile.red = rs
    outfile.green = gs
    outfile.blue = bs
    outfile.header.offset = [0, 0, 0]
    outfile.header.scale = [0.01,0.01,0.01]
    outfile.header.min = [xmin, ymin, zmin]
    outfile.header.max = [xmax, ymax, zmax]
    outfile.x = xs
    outfile.y = ys
    outfile.z = zs
    outfile.close()
    print("Las file written.")

def convertPotree():
    global cloudId
    # Convert update
    timestamp = str(int(round(time.time() * 1000)))
    outputDirectory = dirpath + "/output_updates/" + timestamp
    sourcePath = dirpath + "/las/points.las"
    convert(outputDirectory, sourcePath, incremental=False)
    cloudId = timestamp
    print("Potree conversion done.")

if __name__ == '__main__':
    os.system("rm -rf " + dirpath + "/output_updates/*")
    app.run(debug = True, port=PORT)
