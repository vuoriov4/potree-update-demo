{
    "version": "1.8",
    "octreeDir": "data",
    "projection": "",
    "points": 18521,
    "boundingBox": {
        "lx": -2.56,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 2.8000000000000004,
        "uy": 2.9000000000000005,
        "uz": 5.2
    },
    "tightBoundingBox": {
        "lx": -2.56,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 2.8000000000000004,
        "uy": 2.69,
        "uz": 0.9500000000000001
    },
    "pointAttributes": "LAS",
    "spacing": 0.0,
    "scale": 0.001,
    "hierarchyStepSize": 5
}