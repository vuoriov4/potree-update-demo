{
    "version": "1.8",
    "octreeDir": "data",
    "projection": "",
    "points": 4311,
    "boundingBox": {
        "lx": 0.29,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 2.81,
        "uy": 0.06000000000000005,
        "uz": 2.36
    },
    "tightBoundingBox": {
        "lx": 0.29,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 2.64,
        "uy": 0.06,
        "uz": 0.9400000000000001
    },
    "pointAttributes": "LAS",
    "spacing": 0.0,
    "scale": 0.001,
    "hierarchyStepSize": 5
}