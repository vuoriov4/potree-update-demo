{
    "version": "1.8",
    "octreeDir": "data",
    "projection": "",
    "points": 8569,
    "boundingBox": {
        "lx": 0.05,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 4.87,
        "uy": 2.3600000000000005,
        "uz": 4.66
    },
    "tightBoundingBox": {
        "lx": 0.05,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 2.8000000000000004,
        "uy": 2.36,
        "uz": 0.9400000000000001
    },
    "pointAttributes": "LAS",
    "spacing": 0.0,
    "scale": 0.001,
    "hierarchyStepSize": 5
}