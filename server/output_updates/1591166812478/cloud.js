{
    "version": "1.8",
    "octreeDir": "data",
    "projection": "",
    "points": 96709,
    "boundingBox": {
        "lx": -2.72,
        "ly": -2.84,
        "lz": -0.16,
        "ux": 2.82,
        "uy": 2.7,
        "uz": 5.38
    },
    "tightBoundingBox": {
        "lx": -2.72,
        "ly": -2.84,
        "lz": -0.16,
        "ux": 2.8000000000000004,
        "uy": 2.7,
        "uz": 1.04
    },
    "pointAttributes": "LAS",
    "spacing": 0.0,
    "scale": 0.001,
    "hierarchyStepSize": 5
}