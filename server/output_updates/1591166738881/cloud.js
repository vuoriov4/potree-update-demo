{
    "version": "1.8",
    "octreeDir": "data",
    "projection": "",
    "points": 75455,
    "boundingBox": {
        "lx": -2.72,
        "ly": -2.84,
        "lz": -0.16,
        "ux": 2.809999999999999,
        "uy": 2.6899999999999997,
        "uz": 5.369999999999999
    },
    "tightBoundingBox": {
        "lx": -2.72,
        "ly": -2.84,
        "lz": -0.16,
        "ux": 2.8000000000000004,
        "uy": 2.69,
        "uz": 1.04
    },
    "pointAttributes": "LAS",
    "spacing": 0.0,
    "scale": 0.001,
    "hierarchyStepSize": 5
}