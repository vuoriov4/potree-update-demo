{
    "version": "1.8",
    "octreeDir": "data",
    "projection": "",
    "points": 12925,
    "boundingBox": {
        "lx": 0.02,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 5.17,
        "uy": 2.6900000000000006,
        "uz": 4.99
    },
    "tightBoundingBox": {
        "lx": 0.02,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 2.8000000000000004,
        "uy": 2.69,
        "uz": 0.9500000000000001
    },
    "pointAttributes": "LAS",
    "spacing": 0.0,
    "scale": 0.001,
    "hierarchyStepSize": 5
}