{
    "version": "1.8",
    "octreeDir": "data",
    "projection": "",
    "points": 9554,
    "boundingBox": {
        "lx": 0.05,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 5.0,
        "uy": 2.49,
        "uz": 4.79
    },
    "tightBoundingBox": {
        "lx": 0.05,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 2.8000000000000004,
        "uy": 2.49,
        "uz": 0.9500000000000001
    },
    "pointAttributes": "LAS",
    "spacing": 0.0,
    "scale": 0.001,
    "hierarchyStepSize": 5
}