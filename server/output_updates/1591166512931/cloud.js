{
    "version": "1.8",
    "octreeDir": "data",
    "projection": "",
    "points": 17367,
    "boundingBox": {
        "lx": -1.93,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 3.2200000000000008,
        "uy": 2.6900000000000006,
        "uz": 4.99
    },
    "tightBoundingBox": {
        "lx": -1.93,
        "ly": -2.46,
        "lz": -0.16,
        "ux": 2.8000000000000004,
        "uy": 2.69,
        "uz": 0.9500000000000001
    },
    "pointAttributes": "LAS",
    "spacing": 0.0,
    "scale": 0.001,
    "hierarchyStepSize": 5
}