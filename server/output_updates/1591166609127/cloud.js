{
    "version": "1.8",
    "octreeDir": "data",
    "projection": "",
    "points": 43604,
    "boundingBox": {
        "lx": -2.72,
        "ly": -2.7,
        "lz": -0.16,
        "ux": 2.8000000000000004,
        "uy": 2.8200000000000005,
        "uz": 5.36
    },
    "tightBoundingBox": {
        "lx": -2.72,
        "ly": -2.7,
        "lz": -0.16,
        "ux": 2.8000000000000004,
        "uy": 2.69,
        "uz": 1.0
    },
    "pointAttributes": "LAS",
    "spacing": 0.0,
    "scale": 0.001,
    "hierarchyStepSize": 5
}